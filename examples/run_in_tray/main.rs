#[macro_use]
extern crate tileme;

use std::collections::HashMap;
use tileme::{Config, KeyCode, run_tray};

#[allow(unused_imports)]
fn main() {
    let mut keybindings = HashMap::new();
    {
        use tileme::hotkey::{keys::*, modifiers::*};
        keybindings.insert(
            KeyCode { mask: ALT | SHIFT, code: 'Q' as u32 },
            run_internal!(exit)
        );
        keybindings.insert(
            KeyCode { mask: ALT | SHIFT, code: 'W' as u32 },
            run_internal!(echo, "Alt+Shift+W pressed")
        );
    }
    run_tray(Config::default(), keybindings)
}
