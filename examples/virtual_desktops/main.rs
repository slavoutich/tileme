#[macro_use]
extern crate tileme;

use std::collections::HashMap;
use tileme::{Config, KeyCode, run};


#[allow(unused_imports)]
fn main() {
    let desktops = vec!("A", "S", "D", "F", "G");
    let mut keybindings = HashMap::new();
    {
        use tileme::hotkey::{keys::*, modifiers::*};
        keybindings.insert(
            KeyCode { mask: ALT | SHIFT, code: 'Q' as u32 },
            run_internal!(exit)
        );
        keybindings.insert(
            KeyCode { mask: ALT | SHIFT, code: 'W' as u32 },
            run_internal!(echo, "Alt+Shift+W pressed")
        );
        for (i, desktop) in desktops.iter().enumerate() {
            keybindings.insert(
                KeyCode { mask: ALT, code: desktop.chars().next().unwrap() as u32 },
                run_internal!(go_to_virtual_desktop, i)
            );
            keybindings.insert(
                KeyCode { mask: ALT | SHIFT, code: desktop.chars().next().unwrap() as u32 },
                run_internal!(move_window_to_desktop, i)
            );
        }
    }
    let config = Config {
        virtual_desktops: desktops,
    };
    run(config, keybindings)
}
