use crate::data_types::HWND;
use std::fmt;
use winapi::um::winuser::EnumWindows;
use winapi::shared::minwindef::LPARAM;
use winvd::helpers::get_desktop_number_by_window;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum WindowError {
    #[error("Failed to make a WinAPI call")]
    WinApiError,

    #[error("Failed to make a winvd call")]
    WinvdError,
}

#[derive(Debug)]
pub struct Window {
    hwnd: HWND,
}

impl Window {
    pub fn init(hwnd: HWND) -> Window {
        Window {
            hwnd,
        }
    }

    pub fn desktop_number(&self) -> Result<usize, WindowError> {
        get_desktop_number_by_window(self.hwnd.into())
            .map_err(|_| WindowError::WinvdError)

    }

    pub fn hwnd(&self) -> HWND {
        self.hwnd.clone()
    }
}

impl fmt::Display for Window {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Window ({})", self.hwnd)
    }
}

pub fn get_all_windows() -> Result<Vec<Window>, WindowError> {
    let mut windows = Vec::<Window>::new();
    match unsafe {
        EnumWindows(Some(_get_all_windows_callback), &mut windows as *mut _ as LPARAM)
    } {
        0 => Err(WindowError::WinApiError),
        _ => Ok(windows),
    }
}

unsafe extern "system" fn _get_all_windows_callback(hwnd: winapi::shared::windef::HWND,
                                                    vec_pointer: LPARAM) -> i32 {
    let windows = vec_pointer as *mut Vec<Window>;
    windows.as_mut().unwrap().push(Window::init(hwnd.into()));
    1
}
