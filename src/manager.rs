use crate::KeyCode;
use crate::data_types::{KeyBindings, Config, HWND};
use crate::listeners::winevent::WinEventListener;
use crate::ring::{Ring, Selector};
use crate::virtual_desktop::VirtualDesktop;
use crate::windows::{get_all_windows, Window};
use crossbeam_channel::{Receiver, bounded, Sender};
use log::{info, debug, error};
use std::collections::HashMap;
use std::thread;
use winapi::um::winuser::GetForegroundWindow;
use winvd::helpers::{
    get_desktop_number_by_window,
    go_to_desktop_number,
    move_window_to_desktop_number,
    rename_desktop_number,
};
use winvd;

pub enum Event {
    KeyPress(KeyCode),
    VirtualDesktopEvent(winvd::VirtualDesktopEvent),
    WinEvent(WinEvent),
}

pub enum WinEvent {
    Foreground(HWND),
    MinimizeStart(HWND),
    MinimizeEnd(HWND),
    MoveSizeStart(HWND),
    MoveSizeEnd(HWND),
}

pub struct WindowManager {
    event_sender: Sender<Event>,
    event_receiver: Receiver<Event>,
    threads: Vec<thread::JoinHandle<()>>,
    virtual_desktops: Ring<VirtualDesktop>,
    virtual_desktop_ids: Vec<winvd::Desktop>,
    hwnd_desktop: HashMap<HWND, usize>,
}

impl WindowManager {
    pub fn init(config: Config) -> WindowManager {
        let (sender, receiver) = bounded::<Event>(0);
        let mut wm = WindowManager {
            event_sender: sender,
            event_receiver: receiver,
            threads: Vec::new(),
            virtual_desktops: Ring::new(config.virtual_desktops.iter()
                .map(|name| VirtualDesktop::new(*name)).collect()),
            virtual_desktop_ids: winvd::get_desktops().unwrap(),
            hwnd_desktop: HashMap::new(),
        };

        wm.init_virtual_desktops();
        wm.init_windows();

        wm
    }

    pub fn listen(mut self, mut keybindings: KeyBindings) {
        let hotkey_list = keybindings.iter().map(|(key, _)| key.clone()).collect();
        self.init_virtual_desktop_event_listener();
        self.init_keyboard_listener(hotkey_list);
        self.init_window_event_listener();

        loop {
            match self.event_receiver.recv().unwrap() {
                Event::KeyPress(key_code) =>
                    keybindings.get_mut(&key_code).unwrap()(&mut self),
                Event::VirtualDesktopEvent(event) =>
                    self.handle_virtual_desktop_event(event),
                Event::WinEvent(event) => self.handle_win_event(event),
            };
        }
    }

    fn init_keyboard_listener(&mut self, hotkey_list: Vec<KeyCode>) {
        let event_sender = self.event_sender.clone();
        self.threads.push(thread::spawn(move || {
            let mut hk = hotkey::Listener::new();
            for keycode in hotkey_list {
                let sender = event_sender.clone();
                hk.register_hotkey(
                    keycode.mask, keycode.code,
                    move || sender.send(Event::KeyPress(keycode)).unwrap()
                ).unwrap();
            }
            hk.listen();
        }));
    }

    // Virtual desktops
    fn init_virtual_desktop_event_listener(&mut self) {
        let event_sender = self.event_sender.clone();
        self.threads.push(thread::spawn(move || {
            let receiver = winvd::get_event_receiver();
            loop {
                match receiver.recv() {
                    Ok(event) => {
                        event_sender
                            .send(Event::VirtualDesktopEvent(event))
                            .unwrap_or_else(|err| error!(
                                "Failed to forward a VirtualDesktopEvent to WindowManager: {}",
                                err));
                    },
                    Err(err) => {
                        error!("Failed to receive a VirtualDesktopEvent: {:?}", err);
                    },
                }
            }
        }));
    }

    fn init_window_event_listener(&mut self) {
        let sender = self.event_sender.clone();
        self.threads.push(thread::spawn(move || {
            WinEventListener::init(sender).listen();
        }));
    }

    fn handle_virtual_desktop_event(&mut self, event: winvd::VirtualDesktopEvent) {
        use winvd::VirtualDesktopEvent;
        match event {
            VirtualDesktopEvent::DesktopChanged(_old, new) => self.handle_desktop_changed(&new),
            VirtualDesktopEvent::DesktopCreated(desk) =>
                error!("New desktop created {:?}.", desk),
            VirtualDesktopEvent::DesktopDestroyed(desk) =>
                error!("Desktop {:?} destroyed.", desk),
            VirtualDesktopEvent::WindowChanged(hwnd) => self.handle_window_changed(hwnd.into()),
        };
    }

    fn handle_desktop_changed(&mut self, new: &winvd::Desktop) {
        match self.virtual_desktop_ids.iter().position(|desktop: &winvd::Desktop| desktop == new) {
            Some(vd_index) => { self.virtual_desktops.focus(&Selector::Index(vd_index)); },
            None => error!("Virtual desktop changed to {:?}, but it was not present on the program \
                            startup. Manual manipulation of virtual desktops is not supported. \
                            Program will continue, but inconsistencies may arise.", new),
        }
    }

    /// Update window information, when a WindowChanged event is received for HWND
    fn handle_window_changed(&mut self, hwnd: HWND) {
        match self.hwnd_desktop.get(&hwnd) {
            Some(old_desktop_number) => {
                // Check if window didn't change desktop, if it did - move it.
                // If no new desktop -- remove window.
                match get_desktop_number_by_window(hwnd.into()) {
                    Ok(new_desktop_number) => {
                        debug!("Window {} (may be) moved from desktop {} to desktop {}",
                              hwnd, old_desktop_number, new_desktop_number);
                        if *old_desktop_number != new_desktop_number {
                            // Window moved to the new desktop
                            let window = self.virtual_desktops
                                .get_mut(*old_desktop_number)
                                .expect(format!(
                                    "Internal error: no desktop number {}", old_desktop_number)
                                    .as_str())
                                .pull_window_by_hwnd(hwnd)
                                .expect(format!(
                                    "Internal error: window {} is found in self.hwnd_desktop on \
                                     the desktop number {}, but the correspondent window is not \
                                     present in the virtual desktop ring.",
                                    hwnd, old_desktop_number).as_str());
                            match self.virtual_desktops.get_mut(new_desktop_number) {
                                None => {
                                    error!(
                                        "Could not find virtual desktop number {} in the \
                                        configuration. Probably this is because virtual desktops \
                                        were manipulated manually.", new_desktop_number);
                                    self.hwnd_desktop.remove(&hwnd);
                                }
                                Some(vd) => {
                                    vd.push_window(window);
                                    self.hwnd_desktop.insert(hwnd, new_desktop_number);
                                }
                            }
                        }
                    }
                    Err(_) => {
                        // No new desktop: window is probably destroyed
                        debug!("Destroyed window {}", hwnd);
                        self.virtual_desktops
                            .get_mut(*old_desktop_number)
                            .expect(format!("Internal error: no desktop number {}",
                                            old_desktop_number).as_str())
                            .pull_window_by_hwnd(hwnd)
                            .expect(format!(
                                "Internal error: window {} is found in self.hwnd_desktop on \
                                the desktop number {}, but the correspondent window is not \
                                present in the virtual desktop ring.",
                                hwnd, old_desktop_number).as_str());
                        self.hwnd_desktop.remove(&hwnd);
                    }
                }
            }
            None => {
                // New window created, insert it into the virtual desktop ring
                match get_desktop_number_by_window(hwnd.into()) {
                    Ok(desktop_number) => {
                        debug!("Created new window {} on desktop {}", hwnd, desktop_number);
                        let window = Window::init(hwnd);
                        match self.virtual_desktops.get_mut(desktop_number) {
                            None => error!(
                                "{} was created on a desktop number {}, but it there are only {} \
                                desktops in the configuration. Probably this is because virtual \
                                desktops were manipulated manually.", window, desktop_number,
                                self.virtual_desktops.len()),
                            Some(vd) => {
                                vd.push_window(window);
                                // EVENT_SYSTEM_FOREGROUND arrives earlier than virtual desktop's
                                // window changed event, so at the moment of its arrival window
                                // manager won't have info about the window and won't handle the
                                // event. That's why we must check that after new window creation
                                // foreground window is set correctly.
                                vd.update_focus().unwrap_or_else(|_| debug!(
                                    "Focus is not on any window of the desktop, where new window \
                                    was created"));
                                self.hwnd_desktop.insert(hwnd, desktop_number);
                            }
                        }
                    },
                    Err(err) => debug!(
                        "Created new window {}, but could not determine its desktop: {:?}",
                        hwnd, err),
                }
            }
        }
    }

    fn handle_win_event(&mut self, event: WinEvent) {
        match event {
            WinEvent::Foreground(hwnd) => {
                match self.hwnd_desktop.get(&hwnd) {
                    Some(desktop_number) => {
                        debug!("Window {} went foreground", hwnd);
                        match self.virtual_desktops.get_mut(*desktop_number) {
                            None => error!(
                                "Window with {} went foreground {}, but it there are only {} \
                                desktops in the configuration. Probably this is because virtual \
                                desktops were manipulated manually.", hwnd, desktop_number,
                                self.virtual_desktops.len()),
                            Some(vd) => vd
                                .set_focus_by_hwnd(hwnd.into())
                                .unwrap_or_else(|err| error!(
                                    "Failed to set window {} on desktop number {} foreground: {:?}",
                                    hwnd, desktop_number, err)),
                        }
                    }
                    None => debug!(
                        "Window {} went foreground, but it is not present in \
                         self.hwnd_desktop mapping", hwnd
                    ),
                };
            }
            WinEvent::MinimizeStart(hwnd) => {
                match self.hwnd_desktop.get_mut(&hwnd) {
                    Some(desktop_number) => {
                        debug!(
                            "Window {} was minimized", hwnd
                        );
                        let vd = self.virtual_desktops
                            .get_mut(*desktop_number)
                            .expect(format!(
                                "Internal error: desktop for window {} (number {}) is present in \
                                self.hwnd_desktop, but this desktop is not present in \
                                self.virtual_desktops ring. Consistency of these two data \
                                structures should be checked in code.", hwnd, desktop_number
                            ).as_str());
                        vd.set_minimized(hwnd).unwrap_or_else(|err| error!(
                            "Failed to set window {} on desktop {:?} minimized: {:?}",
                            hwnd, vd, err));
                        vd.update_focus().unwrap_or_else(|_| debug!(
                            "Focus is not on any window of the desktop, where new window was \
                            created"));
                    }
                    None => debug!(
                        "Window {} was minimized, but it is not present in \
                         self.hwnd_desktop mapping", hwnd
                    ),
                }
            },
            WinEvent::MinimizeEnd(hwnd) => {
                match self.hwnd_desktop.get_mut(&hwnd) {
                    Some(desktop_number) => {
                        debug!(
                            "Window {} was unminimized", hwnd
                        );
                        let vd = self.virtual_desktops
                            .get_mut(*desktop_number)
                            .expect(format!(
                                "Internal error: desktop for window {} (number {}) is present in \
                                self.hwnd_desktop, but this desktop is not present in \
                                self.virtual_desktops ring. Consistency of these two data \
                                structures should be checked in code.", hwnd, desktop_number
                            ).as_str());
                        vd.set_unminimized(hwnd).unwrap_or_else(|err| error!(
                            "Failed to set window {} on desktop {:?} unminimized: {:?}",
                            hwnd, vd, err));
                        vd.update_focus().unwrap_or_else(|_| debug!(
                            "Focus is not on any window of the desktop, where new window was \
                            created"));
                    }
                    None => debug!(
                        "Window {} was unminimized, but it is not present in \
                         self.hwnd_desktop mapping", hwnd
                    ),
                }
            },
            WinEvent::MoveSizeStart(_) => {}
            WinEvent::MoveSizeEnd(_) => {}
        }
    }

    fn init_virtual_desktops(&mut self) {
        let n_desktops_current = self.virtual_desktop_ids.len();
        let n_desktops = self.virtual_desktops.len();
        if n_desktops_current < n_desktops {
            for _ in 0..(n_desktops - n_desktops_current) {
                self.virtual_desktop_ids.push(winvd::create_desktop().unwrap());
            }
        }
        for (i, desktop) in self.virtual_desktops.iter().enumerate() {
            rename_desktop_number(i, &*desktop.name).unwrap();
        }
    }

    fn init_windows(&mut self) {
        let windows = get_all_windows().unwrap();
        for window in windows {
            match window.desktop_number() {
                Ok(n) => {
                    debug!("Found window {} on desktop {}", window.hwnd(), n);
                    self.hwnd_desktop.insert(window.hwnd(), n);
                    match self.virtual_desktops.get_mut(n) {
                        Some(vd) => vd.push_window(window),
                        None => debug!("Found a window on the desktop number {}, but the \
                            configuration provides that there are only {} desktops. \
                            Window will be ignored.", n, self.virtual_desktops.len())
                    }
                },
                Err(_) => {},
            }
        }
        let focused_window: HWND = unsafe { GetForegroundWindow() }.into();
        let focused_window_desktop = self.hwnd_desktop
            .get(&focused_window)
            .expect("Focused window was not enumerated in all windows.");
        self.virtual_desktops
            .get_mut(*focused_window_desktop)
            .expect("Focused window desktop is larger than expected number of desktops")
            .set_focus_by_hwnd(focused_window)
            .unwrap_or_else(|err| error!("Failed to set focus: {:?}", err));
    }

    // WINDOW MANAGER ACTIONS

    /// Shut down the WindowManager, running any required cleanup
    pub fn exit(&mut self) {
        std::process::exit(0)
    }

    /// Go to virtual desktop number
    pub fn go_to_virtual_desktop(&mut self, number: usize) {
        match go_to_desktop_number(number) {
            Ok(_) => {
                match self.virtual_desktops.get(number) {
                    Some(vd) => vd.grab_focus().unwrap_or_else(
                        |err| error!("Failed to grab window focus on virtual \
                                                     desktop {:?}: {:?}", vd, err)),
                    None => error!("Virtual desktop changed to number {}, but configuration \
                                    sets that there are only {} desktops. Program will \
                                    continue, but inconsistencies may arise.",
                                   number, self.virtual_desktops.len()),
                }
            }
            Err(err) => error!("Could not go to the desktop number {}: {:?}", number, err),
        }
    }

    /// Move foreground window to desktop number
    pub fn move_window_to_desktop(&mut self, number: usize) {
        let foreground_window: HWND = unsafe { GetForegroundWindow() }.into();
        if let Err(err) = move_window_to_desktop_number(foreground_window.into(), number) {
            error!("Could not move window {} to desktop number {}: {:?}",
                   foreground_window, number, err);
        }
    }

    /// Echo some string, for testing
    pub fn echo(&self, str: &str) {
        info!("{}", str);
    }
}
