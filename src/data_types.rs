use crate::manager::WindowManager;
use std::collections::HashMap;
use std::convert::TryFrom;
use std::fmt;

pub type KeyListenerID = i32;

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
pub struct KeyCode {
    /// Modifier key bit mask
    pub mask: u32,
    /// key code
    pub code: u32,
}

/// Some action to be run by a user key binding
pub type FireAndForget = Box<dyn FnMut(&mut WindowManager) -> () + Send>;

/// User defined key bindings
pub type KeyBindings = HashMap<KeyCode, FireAndForget>;


pub struct Config<'a> {
    /// Workspace names to use when initialising the WindowManager. Must have at least one element.
    pub virtual_desktops: Vec<&'a str>,
}

impl<'a> Config<'a> {
    /// Initialise a default Config, giving sensible (but minimal) values for all fields.
    pub fn default() -> Config<'a> {
        Config {
            virtual_desktops: vec!["Desktop 1"],
        }
    }
}

#[derive(Debug, Hash, Clone, PartialEq, Eq, Copy)]
pub struct HWND {
    hwnd: usize,
}

impl From<winvd::HWND> for HWND {
    fn from(hwnd: u32) -> Self {
        HWND { hwnd: hwnd as usize }
    }
}

impl From<winapi::shared::windef::HWND> for HWND {
    fn from(hwnd: winapi::shared::windef::HWND) -> Self {
        HWND { hwnd: hwnd as usize }
    }
}

impl Into<winvd::HWND> for HWND {
    fn into(self) -> u32 {
        u32::try_from(self.hwnd).expect("Could not convert usize to u32: \
                                           number is too large")
    }
}

impl Into<winapi::shared::windef::HWND> for HWND {
    fn into(self) -> winapi::shared::windef::HWND {
        self.hwnd as winapi::shared::windef::HWND
    }
}

impl fmt::Display for HWND {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "HWND {{ {:#x} }}", self.hwnd)
    }
}

#[derive(Debug)]
pub struct Rect {
    left: isize,
    top: isize,
    right: isize,
    bottom: isize,
}

impl Rect {
    pub fn from_ltrb(left: isize, top: isize, right: isize, bottom: isize) -> Self {
        Rect { left, top, right, bottom }
    }
}

impl fmt::Display for Rect {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}x{}..{}x{}", self.left, self.top, self.right, self.bottom)
    }
}
