#![cfg(feature = "tray")]
#![windows_subsystem = "windows"]


extern crate native_windows_derive as nwd;
extern crate native_windows_gui as nwg;

use nwd::NwgUi;
use nwg::NativeUi;
use std::error::Error;
use std::thread;
use crate::{WindowManager, Config};
use crate::data_types::KeyBindings;

const ICON: &[u8] = include_bytes!("../assets/icon_32x32.ico");

#[derive(Default, NwgUi)]
pub struct SystemTray {
    #[nwg_resource(source_bin: Some(ICON))]
    icon: nwg::Icon,

    #[nwg_control]
    window: nwg::MessageWindow,

    #[nwg_control(icon: Some(&data.icon))]
    #[nwg_events(OnMousePress: [SystemTray::show_menu])]
    tray: nwg::TrayNotification,

    #[nwg_control(parent: window, popup: true)]
    tray_menu: nwg::Menu,

    #[nwg_control(parent: tray_menu, text: "Exit")]
    #[nwg_events(OnMenuItemSelected: [SystemTray::exit])]
    tray_item_exit: nwg::MenuItem,
}

impl SystemTray {
    fn show_menu(&self) {
        let (x, y) = nwg::GlobalCursor::position();
        self.tray_menu.popup(x, y);
    }

    fn exit(&self) {
        nwg::stop_thread_dispatch();
    }

}

pub(crate) fn run(config: Config<'static>, keybindings: KeyBindings) -> Result<(), Box<dyn Error>> {
    nwg::init()?;
    let ui = SystemTray::build_ui(Default::default())?;
    ui.tray.set_icon(&ui.icon);
    ui.tray.set_tip("TileMe");
    thread::spawn(move || {
        WindowManager::init(config).listen(keybindings)
    });
    nwg::dispatch_thread_events();
    Ok(())
}
