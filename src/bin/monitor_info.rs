use tileme::monitors::get_monitors;

fn main() {
    let monitors = get_monitors().unwrap();
    println!("Detected {} monitors:", monitors.len());
    for (i, mon) in (1..).zip(monitors.iter()) {
        let primary = if mon.primary() { "primary" } else { "secondary" };
        println!("{}. ({}) ID: {}", i, primary, mon.id());
        println!("    Full rectangle: {}", mon.rect());
        println!("    Work rectangle: {}", mon.work_area());
    }
}
