use crate::data_types::Rect;
use std::convert::TryFrom;
use std::ffi::OsString;
use std::mem::{MaybeUninit, size_of};
use std::ptr::{null_mut, addr_of_mut};
use thiserror::Error;
use winapi::shared::minwindef::{LPARAM, BOOL, DWORD};
use winapi::shared::windef::{RECT, HMONITOR, LPRECT, HDC};
use winapi::um::wingdi::{DISPLAY_DEVICEW};
use winapi::um::winuser::{EnumDisplayMonitors, GetMonitorInfoW, MONITORINFOEXW, LPMONITORINFO,
                          MONITORINFOF_PRIMARY, EnumDisplayDevicesW, EDD_GET_DEVICE_INTERFACE_NAME};
use std::os::windows::ffi::OsStringExt;
use std::fmt;

#[derive(Error, Debug)]
pub enum MonitorError {
    #[error("Failed to make a WinAPI call to function `{0}`")]
    WinApiError(String),

    #[error("Failed to decode UTF16 string")]
    Utf16DecodeError,
}

#[derive(Debug)]
pub struct MonitorID {
    // id: [u16; 128],
    id: OsString
}

impl MonitorID {
    pub fn new(id: &[u16]) -> Self {
        let mut end = 0;
        for (i, char) in id.iter().enumerate().rev() {
            if *char != 0 {
                end = i+1;
                break
            }
        }
        Self { id: OsString::from_wide(&id[..end]) }
    }
}

impl fmt::Display for MonitorID {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let repr = self.id.to_str().ok_or(fmt::Error)?;
        f.write_str(repr)
    }
}

#[derive(Debug)]
pub struct Monitor {
    id: MonitorID,
    rect: Rect,
    work_area: Rect,
    primary: bool,
}


impl Monitor {
    fn new(
       id: MonitorID, rect: Rect, work_area: Rect, primary: bool
    ) -> Self {
        Self { id, rect, work_area, primary }
    }

    pub fn id(&self) -> &MonitorID {
        &self.id
    }

    pub fn rect(&self) -> &Rect {
        &self.rect
    }

    pub fn work_area(&self) -> &Rect {
        &self.work_area
    }

    pub fn primary(&self) -> bool {
        self.primary
    }
}


impl From<RECT> for Rect {
    fn from(rect: RECT) -> Self {
        Self::from_ltrb(
            isize::try_from(rect.left).unwrap(),
            isize::try_from(rect.top).unwrap(),
            isize::try_from(rect.right).unwrap(),
            isize::try_from(rect.bottom).unwrap(),
        )
    }
}

pub fn get_monitors() -> Result<Vec<Monitor>, MonitorError>{
    get_monitor_handlers()?
        .iter()
        .map(|h| get_monitor(*h))
        .collect()
}

fn get_monitor(hmonitor: HMONITOR) -> Result<Monitor, MonitorError> {
    let mi = get_monitor_info_ex_w(hmonitor)?;
    let dd = get_display_device_w(mi.szDevice.as_ref(), EDD_GET_DEVICE_INTERFACE_NAME)?;
    Ok(Monitor::new(
        MonitorID::new(&dd.DeviceID),
        mi.rcMonitor.into(),
        mi.rcWork.into(),
        mi.dwFlags & MONITORINFOF_PRIMARY != 0,
    ))
}

fn get_monitor_handlers() -> Result<Vec<HMONITOR>, MonitorError> {
    let mut monitors: Vec<HMONITOR> = Vec::new();
    match unsafe { EnumDisplayMonitors(
        null_mut(), null_mut(), Some(get_monitor_handlers_callback),
        &mut monitors as *mut _ as LPARAM
    )} {
        0 => Err(MonitorError::WinApiError("EnumDisplayMonitors".into())),
        _ => Ok(monitors)
    }
}

fn get_monitor_info_ex_w(hmonitor: HMONITOR) -> Result<MONITORINFOEXW, MonitorError> {
    let mut mi: MaybeUninit<MONITORINFOEXW> = MaybeUninit::uninit();
    let lpmi = mi.as_mut_ptr();
    unsafe {
        addr_of_mut!((*lpmi).cbSize).write(u32::try_from(size_of::<MONITORINFOEXW>()).unwrap());
        match GetMonitorInfoW(hmonitor, lpmi as LPMONITORINFO) {
            0 => Err(MonitorError::WinApiError(format!("GetMonitorInfoW(hmonitor: {:?})", hmonitor))),
            _ => Ok(mi.assume_init())
        }
    }
}

fn get_display_device_w(
    device: &[u16], flags: DWORD
) -> Result<DISPLAY_DEVICEW, MonitorError> {
    let mut dd = MaybeUninit::<DISPLAY_DEVICEW>::uninit();
    let lpdd = dd.as_mut_ptr();
    unsafe {
        addr_of_mut!((*lpdd).cb).write(u32::try_from(size_of::<DISPLAY_DEVICEW>()).unwrap());
        match EnumDisplayDevicesW(device.as_ptr(), 0,
                                lpdd, flags) {
            0 => Err(MonitorError::WinApiError(
                format!("EnumDisplayDevicesW(lpDevice: {})", String::from_utf16_lossy(device)))
            ),
            _ => Ok(dd.assume_init())
        }
    }
}

unsafe extern "system" fn get_monitor_handlers_callback(hmonitor: HMONITOR, _: HDC,
                                                        _: LPRECT, lparam: LPARAM) -> BOOL {
    let monitors_ptr = lparam as *mut Vec<HMONITOR>;
    monitors_ptr.as_mut().unwrap().push(hmonitor);
    1
}

