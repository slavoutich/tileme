use crate::data_types::HWND;
use crate::windows::Window;
use crate::ring::{Ring, Selector};
use thiserror::Error;
use winapi::um::winuser::{SetForegroundWindow, GetForegroundWindow, IsIconic};
use log::debug;

#[derive(Error,Debug)]
pub enum VirtualDesktopError {
    #[error("Window is not present on a virtual desktop number")]
    WindowAbsent,

    #[error("Failed to grab focus on a focused window")]
    GrabFocusError,
}


#[derive(Debug)]
pub struct VirtualDesktop {
    pub(crate) name: String,
    windows: Ring<Window>,
    minimized: Ring<Window>,
}


impl PartialEq for VirtualDesktop {
    fn eq(&self, other: &Self) -> bool {
        // TODO: better way to handle it?
        self.name == other.name
    }
}


impl VirtualDesktop {
    /// Construct a new Workspace with the given name and choice of Layouts
    pub fn new(name: impl Into<String>) -> VirtualDesktop {
        VirtualDesktop {
            name: name.into(),
            windows: Ring::new(Vec::new()),
            minimized: Ring::new(Vec::new()),
        }
    }

    /// Add a window to virtual desktop
    pub fn push_window(&mut self, window: Window) {
        let minimized = unsafe { IsIconic(window.hwnd().into()) } > 0;
        if minimized {
            self.minimized.push(window);
        } else {
            self.windows.push(window)
        }
    }

    /// Remove a window from virtual desktop
    pub fn pull_window_by_hwnd(&mut self, hwnd: HWND) -> Option<Window> {
        let condition = |window: &Window| window.hwnd() == hwnd;
        let selector = Selector::Condition(&condition);
        self.windows.remove(&selector).or(self.minimized.remove(&selector))
    }

    /// Sets a focused window on this virtual desktop, if present
    pub fn set_focus_by_hwnd(&mut self, hwnd: HWND) -> Result<(), VirtualDesktopError> {
        let condition = |window: &Window| window.hwnd() == hwnd;
        let selector = Selector::Condition(&condition);
        match self.windows.focus(&selector) {
            None => Err(VirtualDesktopError::WindowAbsent),
            Some(_) => Ok(()),
        }
    }

    pub fn set_minimized(&mut self, hwnd: HWND) -> Result<(), VirtualDesktopError> {
        let condition = |window: &Window| window.hwnd() == hwnd;
        let selector = Selector::Condition(&condition);
        match self.windows.remove(&selector) {
            Some(window) => {
                self.minimized.push(window);
                Ok(())
            },
            None => {
                match self.minimized.index(&selector){
                    None => Err(VirtualDesktopError::WindowAbsent),
                    Some(_) => Ok(()),
                }
            }
        }
    }

    pub fn set_unminimized(&mut self, hwnd: HWND) -> Result<(), VirtualDesktopError>{
        let condition = |window: &Window| window.hwnd() == hwnd;
        let selector = Selector::Condition(&condition);
        match self.minimized.remove(&selector) {
            Some(window) => {
                self.windows.push(window);
                Ok(())
            },
            None => {
                match self.windows.index(&selector) {
                    None => Err(VirtualDesktopError::WindowAbsent),
                    Some(_) => Ok(())
                }
            }
        }
    }

    /// Grab a focus to the focused window of this virtual desktop.
    /// If the virtual desktop has zero windows, this function will do nothing.
    pub fn grab_focus(&self) -> Result<(), VirtualDesktopError> {
        match self.windows.focused() {
            Some(window) => {
                debug!("Grabbing focus on window {}", window);
                match unsafe { SetForegroundWindow(window.hwnd().into()) } {
                    0 => Err(VirtualDesktopError::GrabFocusError),
                    _ => Ok(())
                }
            },
            None => Ok(()),
        }
    }

    /// Ensure that focus is set correctly
    pub(crate) fn update_focus(&mut self) -> Result<(), VirtualDesktopError> {
        self.set_focus_by_hwnd(unsafe{ GetForegroundWindow() }.into())
    }
}