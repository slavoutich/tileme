pub mod data_types;
pub mod keys;
#[macro_use]
pub mod macros;
pub mod manager;
pub mod monitors;
mod ring;
mod virtual_desktop;
mod windows;
mod listeners;

#[cfg(feature = "tray")] mod tray;
mod platforms;

pub use hotkey;
pub use data_types::{Config, KeyCode};

use crate::data_types::KeyBindings;
use manager::WindowManager;
use log::error;


pub fn run(config: Config, keybindings: KeyBindings) {
    WindowManager::init(config).listen(keybindings)
}


#[cfg(feature = "tray")]
pub fn run_tray(config: Config<'static>, keybindings: KeyBindings) {
    match tray::run(config, keybindings) {
        Ok(_) => {},
        Err(err) => error!("Failed to launch tray application: error {}", err)
    }
}
