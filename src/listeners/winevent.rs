use crate::manager::{Event, WinEvent};
use crossbeam_channel::Sender;
use log::error;
use std::cell::RefCell;
use std::mem::MaybeUninit;
use std::ptr::null_mut;
use winapi::shared::minwindef::DWORD;
use winapi::shared::ntdef::LONG;
use winapi::shared::windef::{HWND, HWINEVENTHOOK};
use winapi::um::winuser::{
    DispatchMessageW,
    EVENT_SYSTEM_FOREGROUND,
    EVENT_SYSTEM_MINIMIZESTART,
    EVENT_SYSTEM_MINIMIZEEND,
    GetMessageW,
    MSG,
    SetWinEventHook,
    TranslateMessage,
    WINEVENT_OUTOFCONTEXT,
    WINEVENT_SKIPOWNPROCESS
};

thread_local!(static SENDER: RefCell<*const Sender<Event>> = RefCell::new(null_mut()));

unsafe fn send(event: WinEvent) {
    SENDER.with(|s| {
        s.borrow()
            .as_ref()
            .expect("Processing an event was requested before initialization")
            .send(Event::WinEvent(event))
            .unwrap_or_else(|err| error!("Failed to send window event: {:?}", err));
    })
}

unsafe extern "system" fn _process_system_foreground(
    _: HWINEVENTHOOK,
    _: DWORD,
    hwnd: HWND,
    _: LONG,
    _: LONG,
    _: DWORD,
    _: DWORD,
) {
    send(WinEvent::Foreground(hwnd.into()));
}

unsafe extern "system" fn _process_system_minimize_start(
    _: HWINEVENTHOOK,
    _: DWORD,
    hwnd: HWND,
    _: LONG,
    _: LONG,
    _: DWORD,
    _: DWORD,
) {
    send(WinEvent::MinimizeStart(hwnd.into()));
}

unsafe extern "system" fn _process_system_minimize_end(
    _: HWINEVENTHOOK,
    _: DWORD,
    hwnd: HWND,
    _: LONG,
    _: LONG,
    _: DWORD,
    _: DWORD,
) {
    send(WinEvent::MinimizeEnd(hwnd.into()));
}

pub struct WinEventListener {
    sender: Sender<Event>,
    hooks: Vec<HWINEVENTHOOK>,
}

impl WinEventListener {
    pub fn init(sender: Sender<Event>) -> WinEventListener {
        WinEventListener {
            sender,
            hooks: Vec::new(),
        }
    }

    fn hook(
        &mut self,
        event: DWORD,
        func: unsafe extern "system" fn(HWINEVENTHOOK, DWORD, HWND, LONG, LONG, DWORD, DWORD) -> ()
    ) {
        let out = unsafe {
            SetWinEventHook(
                event,
                event,
                null_mut(),
                Some(func),
                0,
                0,
                WINEVENT_OUTOFCONTEXT | WINEVENT_SKIPOWNPROCESS,
            )
        };
        if out.is_null() {
            panic!("Could not register hook for event");
        };
        self.hooks.push(out);
    }

    pub fn listen(mut self) {
        SENDER.with(|s| {
            if !(*s.borrow()).is_null() {
                panic!("Attempting to initialize WinEventListener twice in the same thread.")
            }
            *s.borrow_mut() = &self.sender as *const Sender<Event>;
        });
        self.hook(EVENT_SYSTEM_FOREGROUND, _process_system_foreground);
        self.hook(EVENT_SYSTEM_MINIMIZESTART, _process_system_minimize_start);
        self.hook(EVENT_SYSTEM_MINIMIZEEND, _process_system_minimize_end);

        unsafe {
            loop {
                let mut msg: MSG = MaybeUninit::uninit().assume_init();
                while GetMessageW(&mut msg, 0 as HWND, 0, 0) > 0 {
                    TranslateMessage(&msg);
                    DispatchMessageW(&msg);
                }
            }
        }
    }
}