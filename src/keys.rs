pub mod modifiers {
    use winapi::um::winuser;

    pub const ALT: u32 = winuser::MOD_ALT as u32;
    pub const CONTROL: u32 = winuser::MOD_CONTROL as u32;
    pub const SHIFT: u32 = winuser::MOD_SHIFT as u32;
    pub const SUPER: u32 = winuser::MOD_WIN as u32;
}

pub mod keys {
    use winapi::um::winuser;

    pub const BACKSPACE: i32 = winuser::VK_BACK;
    pub const TAB: i32 = winuser::VK_TAB;
    pub const ENTER: i32 = winuser::VK_RETURN;
    pub const CAPS_LOCK: i32 = winuser::VK_CAPITAL;
    pub const ESCAPE: i32 = winuser::VK_ESCAPE;
    pub const SPACE: i32 = winuser::VK_SPACE;
    pub const PAGE_UP: i32 = winuser::VK_PRIOR;
    pub const PAGE_DOWN: i32 = winuser::VK_NEXT;
    pub const END: i32 = winuser::VK_END;
    pub const HOME: i32 = winuser::VK_HOME;
    pub const ARROW_LEFT: i32 = winuser::VK_LEFT;
    pub const ARROW_RIGHT: i32 = winuser::VK_RIGHT;
    pub const ARROW_UP: i32 = winuser::VK_UP;
    pub const ARROW_DOWN: i32 = winuser::VK_DOWN;
    pub const PRINT_SCREEN: i32 = winuser::VK_SNAPSHOT;
    pub const INSERT: i32 = winuser::VK_INSERT;
    pub const DELETE: i32 = winuser::VK_DELETE;
}
