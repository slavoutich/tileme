TileMe
======
`tileme` is a WIP (not yet tiling) window manager for Windows 10, implemented in Rust.

The goal of this project is to provide [XMonad](https://xmonad.org/)-style experience in Windows.
The project is on the early stage of development, so for now it is mostly a utility to manage Windows virtual desktops.
As in XMonad, this project is not an executable, but a library to write your own window manager.

What `tileme` can do now
------------------------
- Create a set of Windows virtual desktops on startup.
- Use hotkeys to switch between Windows virtual desktops and move windows around.
- Track focus of windows. Windows doesn't switch focus reliably on desktop switch:
  focus stays on the old window, which is tracked and fixed.

Planned features
----------------
- **Workspaces instead of Windows' virtual desktops.**
  User can define an arbitrary number of workspaces, that contain tiled windows.
  Each screen should have one or more workspace visible.
  Workspace rotation must be independent and should smoothly adapt, if an external screen is connected and disconnected.
- (Far future) **Linux/Wayland** Ideally I would like to have the same configuration on Windows and Linux,
  so if this project survives the time, I would like to build a Wayland compositor on top of it.

How to use
----------
- [Install](https://rustup.rs/) stable Rust compiler.
- Examine [examples](https://gitlab.com/slavoutich/tileme/-/tree/master/examples) of the configuration.
- Create a Rust binary project, that depends on `tileme`, specifying the correct local path.
- Write `main.rs` based on examples, build and execute it.

Acknowledgements
----------------
This project in fact started from the source code of [penrose](https://github.com/sminez/penrose) tiling window manager
for Linux/X11, and contains some copy-pasted code from there.
I would like to thank its author @sminez for the awesome work, and I would happily use it, if I use Linux/X11
(but now I would prefer Linux to go in Wayland direction).
I would like to thank all authors of [XMonad](https://xmonad.org/) for an awesome desktop experience,
that I had all the time I used it.

License
-------
The source code is available under the terms of [MIT license](LICENSE).
